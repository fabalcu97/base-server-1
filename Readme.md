# Back-End Seed

* Remove this heading when developing
* Replace all `{{project_name}}` tags in the project

# {{project_name}}

## Development Setup
* Run `cd` into {{project_name}} folder and run `npm install` to install all the requirements and dependencies.
* Copy the file named `default_environment.yml` in {{project_name}} path and replace the necessary variables.
* REQUIRED: In order to fill the `secretKey` field in the environment file, run `npm run get-secret-key` and copy the generated code(32 bits + dashes).

## {{project_name}} Execution
* Run `npm run build` to get the server transpiled
* Run `npm run dev` to run a watcher on the files so it can be rebuilt on every change.
* To start the server run `npm start`. This will run and reload the server on every change.

## {{project_name}} Testing
* In order to execute tests run `npm run test`.