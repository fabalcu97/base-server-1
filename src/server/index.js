import Express from 'express';
import mongoose from 'mongoose';

import setMiddleware from '../middlewares';
import setRouters from '../routers';
import settings from '../settings';

const app = Express();

const connection = mongoose.connect(settings.databaseURI, { useNewUrlParser: true });

connection.then((dbConnection) => {
  // Set middleware
  setMiddleware(app, dbConnection);

  // Set Routers
  setRouters(app, dbConnection);

  // Start Server
  app.listen(settings.port, settings.hostname, () => {
    console.log(`Server running on http://${settings.hostname}:${settings.port}`);
  });
});

connection.catch((err) => {
  console.error('Error connecting to database. ', err);
});
