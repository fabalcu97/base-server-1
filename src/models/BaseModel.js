import { Schema, model } from 'mongoose';

class BaseModel {
  constructor() {
    this.schema = {};
    this.graphQLSchema = {};
    this.resolvers = {};
  }

  setSchema(schema) {
    this.schema = new Schema(schema);
    this.modelName = Object.getPrototypeOf(this).constructor.name.toLowerCase();
    model(this.modelName, this.schema);
  }

  static getSchema() {
    return this.graphQLSchema;
  }

  static getResolvers() {
    return this.resolvers;
  }
}

export default BaseModel;
