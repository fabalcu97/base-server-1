import { buildSchema } from 'graphql';
import { Types } from 'mongoose';

import BaseModel from './BaseModel';


class User extends BaseModel {
  constructor() {
    super();

    this.setSchema({
      name: String,
      email: String,
      credentialsId: Types.ObjectId,
    });

    this.graphQLSchema = buildSchema(`
      type Query {
        name: String
      }
      type Mutation {
        setName(name: String): String
      }
    `);

    this.resolvers = {
      name: () => 'GetUserName',
      setName: ({ name }) => 'SetUserName',
    };
  }
}

export default User;
