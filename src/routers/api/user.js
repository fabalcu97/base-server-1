import { Router } from 'express';
import graphqlHTTP from 'express-graphql';

import { buildSchema } from 'graphql';

import User from '../../models/User';

const apiRouter = Router({
  caseSensitive: true,
});

// The callback will be executed always in this router
// apiRouter.use(callback);

// The callback will be used always under thihs path
// apiRouter.all('', () => {});

// The callback will be used on every path that contains the param
// apiRouter.param(param, callback);

apiRouter.use('/getMe', graphqlHTTP({
  schema: User.getSchema(),
  rootValue: User.getResolvers(),
  graphiql: true,
  pretty: true,
}));

const user = new User();


export default apiRouter;
