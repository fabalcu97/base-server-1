import apiRouter from './api/user';

export default function SetRouters(expressApp, databaseConnection) {
  expressApp.use('/api', apiRouter);
}
