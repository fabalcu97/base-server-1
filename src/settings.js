import { join } from 'path';
import { readFileSync } from 'fs';
import { safeLoad } from 'js-yaml';

const settingsPath = join(__dirname, '..', 'environment.yml');
const settings = safeLoad(readFileSync(settingsPath, 'utf-8'));

settings.hostname = settings.hostname ? settings.hostname : 'localhost';
const {
  domain, port, user, password, name,
} = settings.database;
settings.databaseURI = `mongodb://${user}:${password}@${domain}:${port}/${name}`;

export default settings;
