import uuidv1 from 'uuid/v1';
import session from 'express-session';
import mongoose from 'mongoose';
import ConnectMongo from 'connect-mongo';

import settings from '../settings';

const MongoStore = ConnectMongo(session);

/**
 * Session configuration.
 * Check here https://www.npmjs.com/package/express-session for more information.
 * Check here about the session storage: https://www.npmjs.com/package/connect-mongo
 */
function sessionConfiguration() {
  return session({
    cookie: {
      path: '/',
      secure: true,
      maxAge: 3600000, // one hour
      httpOnly: !!settings.https,
    },
    resave: true,
    saveUninitialized: false,
    genid: uuidv1,
    name: `${settings.projectName}Id`,
    secret: settings.secretKey,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  });
}

export default sessionConfiguration;
