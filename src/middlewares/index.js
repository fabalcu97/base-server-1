import helmet from 'helmet';
import morgan from 'morgan';

import settings from '../settings';

/**
 * Method that allows setup all the middleware required by the server.
 * @param {Express} expressApp Server to consume middleware
 */
export default function SetMiddleware(expressApp, dbConnection) {
  expressApp.use(helmet());
  expressApp.use(morgan(settings.dev ? 'dev' : 'default'));
}
